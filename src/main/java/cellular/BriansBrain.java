package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }


    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row< currentGeneration.numRows(); row++){
            for (int col=0; col< currentGeneration.numColumns(); col++){
                if (random.nextBoolean()){
                    currentGeneration.set(row, col, CellState.ALIVE);
                }
                else{
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }


    }

    @Override
    public void step() {
    IGrid nextGeneration = currentGeneration.copy();
    for (int row =0; row< numberOfRows(); row++){
        for(int col =0; col < numberOfColumns(); col++){
            nextGeneration.set(row, col, getNextCell(row, col));
        }
    }        
    currentGeneration=nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
    CellState status = CellState.DEAD;
    if ( getCellState(row, col) == CellState.ALIVE){
        status = CellState.DYING;
    }
    else if( getCellState(row, col) == CellState.DYING){
        status = CellState.DEAD;
    }
    else if ( (getCellState(row, col)==CellState.DEAD) && (countNeighbors(row,col, CellState.ALIVE)) == 2 ){
        status = CellState.ALIVE;
    }
    return status;
    
    }

    

    private int countNeighbors(int row, int col, CellState state) {
        int nNeighbors = 0;
        int nRows = numberOfRows();
        int nCols = numberOfColumns();

        //Lager en 3x3 matrise om [row,col]

        for (int i=-1;i<=1; i+=1){
            for (int j=-1;j<=1;j+=1){
                if (i==0 && j==0){
                    continue;
                }
                
                
                else if (i+row>=nRows || i+row<0 || j+col>=nCols || j+col<0) {
                    continue;
                }
                else if (getCellState(row+i, col+j).equals(state)){
                    nNeighbors++;
                }
            }
            
        }
        return nNeighbors;    
        
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    
    
}
