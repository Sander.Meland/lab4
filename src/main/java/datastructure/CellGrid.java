package datastructure;

//import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    CellState[][] grid;
    
    




    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        grid = new CellState[rows][columns];
        for (int row =0 ; row<rows; row++){
            for(int column =0; column<columns; column++){
                grid[row][column] = initialState;
            }
        }
    }
        
        // IGrid grid = new CellGrid(rows, columns, CellState.DEAD);
        //grid = new CellState[rows][columns];
        //this.initialState = dead;
	

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    /**
    public void fill(CellState elem) {
		for(Location loc : locations()) {
			set(loc, elem);
		}
	}
 */


    @Override
    public void set(int row, int column, CellState element) {
    grid[row][column]=element;
    /** if (row >rows || row<0){
            throw new IndexOutOfBoundsException("The element is out of bounds; row = " +row );
        }
        else if (column>columns|| column<0){
            throw new IndexOutOfBoundsException("The element is out of bounds; col = " +column);
        }
         */   
       
            
    }
    

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copied = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for (int row=0; row<grid.length;row++) {
            for (int col=0;col<grid[row].length;col++){
                copied.set(row, col, grid[row][col]);
            }
        }
        return copied;
    }
    }
    

